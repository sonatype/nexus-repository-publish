# Bitbucket Pipelines Pipe: nexus-repository-publish

This Bitbucket pipe allows you to publish your artifacts to Nexus Repository Manager.
The latest version is published as: `sonatype/nexus-repository-publish:0.0.1`

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: sonatype/nexus-repository-publish:0.0.1
  variables:
    FILENAME: '<string>'
    ATTRIBUTES: '<string>'
    USERNAME: '<string>'
    PASSWORD: '<string>'
    SERVER_URL: '<string>'
    # REPOSITORY: '<string>' # Optional. 
    # FORMAT: '<string>' # Optional. 
```

## Variables

| Variable                 | Usage | Example |
| --------------------------- | ----- | ----- |
| FILENAME (*)       | Path to file to publish. | target/demo-java-spring-0.0.1-SNAPSHOT.jar |
| ATTRIBUTES (*)     | Artifact attributes. | -CgroupId=com.example -CartifactId=myapp -Cversion=1.3 -Aextension=jar |
| USERNAME (*)       | Nexus username | admin |
| PASSWORD (*)       | Nexus password | admin123 |
| SERVER_URL (*)     | Nexus server URL | http://nexus.example.com/ |
| REPOSITORY         | Nexus repository name. Default: maven-releases |
| FORMAT             | Artifact format. Default: maven2 |
          
_(*) = required variable._

## Details
Upload your artifacts to Nexus Repository Manager.
* **filename**: path to the file to publish. This is typically the same value as the `path` used in
the `store_artifacts` step in the code example.
* **attributes**: List of component and asset attributes. Component attributes are prefixed with `-C` and asset attributes
 with `-A`. This is passed directly to Nexus Repository Manger so many additional 
 attributes to the ones used in the example above can be used as well.

## Prerequisites

Nexus Repository Manager 3.x is required.

## Examples

```yaml
    - step:
        # set NEXUS_USERNAME and NEXUS_PASSWORD as environment variables
        name: Deploy to Nexus Repository Manager
        deployment: test   # set to test, staging or production
        # trigger: manual  # uncomment to have a manual step
        script:
          - pipe: sonatype/nexus-repository-publish:0.0.1
            variables:
              FILENAME: 'target/myapp-1.0-SNAPSHOT.jar'
              ATTRIBUTES: '-CgroupId=com.example -CartifactId=myapp -Cversion=1.0 -Aextension=jar'
              USERNAME: '$NEXUS_USERNAME'
              PASSWORD: '$NEXUS_PASSWORD'
              SERVER_URL: 'https://nexus.example.com/'
```

A full example project can be found here: https://bitbucket.org/sonatype/nexus-repository-publish-demo/

## Support

If you have any feedback or issues that are not covered here, please join us at the (Sonatype Exchange)[https://community.sonatype.com/c/exchange].
